package com.smarterd.util;

import java.sql.*;
import org.apache.wink.json4j.OrderedJSONObject;

public class SmarteUtil {
	private static String devConnString = "jdbc:mysql://35.197.67.145:3306/SE?useSSL=false";
	private static String preprodConnString = "jdbc:mysql://35.227.181.195:3306/SE?useSSL=false";
	private static String prodConnString = "jdbc:mysql://35.197.106.183:3306/SE?useSSL=false";

	private Connection conn = null;

	public SmarteUtil(String dbEnv, String catalog) throws Exception {
		if(dbEnv.equals("DEV")) {
			conn = DriverManager.getConnection(devConnString, "root", "smarterD2018!");
			conn.setCatalog(catalog);
			conn.setAutoCommit(false);
			System.out.println("Successfully established connection ["+dbEnv+":"+catalog+"]!!!");
		} else if(dbEnv.equals("PREPROD")) {
			conn = DriverManager.getConnection(preprodConnString, "root", "smarterD2018!");
			conn.setCatalog(catalog);
			conn.setAutoCommit(false);
			System.out.println("Successfully established connection ["+dbEnv+":"+catalog+"]!!!");
		} else if(dbEnv.equals("PROD")) {
			conn = DriverManager.getConnection(prodConnString, "root", "smarterD2018!");
			conn.setCatalog(catalog);
			conn.setAutoCommit(false);
			System.out.println("Successfully established connection ["+dbEnv+":"+catalog+"]!!!");
		} else {
			throw new Exception("Invalid DB Envoirnment!!!");
		}
	}

	private void migrateStrategyRelationship() throws Exception {
		int insertedCount = 0, foundCount = 0, orphanCount = 0, reverseCount = 0;
		String sql = "select * from enterprise_strategy_asset_rel";
		String checkSQL = "select count(*) count from enterprise_rel where component_id=? and asset_id=?";
		String insertSQL = "insert into enterprise_rel(id,component_id,asset_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,?,?,?,?)";
		String updateSQL = "update enterprise_rel set asset_rel_context=? where component_id=? and asset_id=?";
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while (rs.next()) {
			String componentId = rs.getString("enterprise_strategy_id");
			String assetId = rs.getString("asset_id");
			String type = rs.getString("asset_rel_type");
			String ctx = rs.getString("asset_rel_context");
			String updatedBy = rs.getString("updated_by");
			String updatedTimestamp = rs.getString("updated_timestamp").toString();
			String companyCode = rs.getString("company_code");

			System.out.println(componentId+":"+assetId+":"+type+":"+ctx);

			String componentType = checkRelationship(componentId);
			
			if(componentType != null) {
				int count = 0;
				PreparedStatement pst1 = conn.prepareStatement(checkSQL);
				pst1.setString(1, componentId);
				pst1.setString(2, assetId);
				ResultSet rs1 = pst1.executeQuery();
				while (rs1.next()) {
					count = rs1.getInt("count");
				}
				rs1.close();
				pst1.close();

				if(count == 0) {
					OrderedJSONObject ctxObj = null;
					if(ctx != null) {
						ctxObj = new OrderedJSONObject(ctx);
					} else {
						ctxObj = new OrderedJSONObject();
					}
					
					if(ctxObj.has("COMPONENT TYPE")) {
						String compType = (String) ctxObj.get("COMPONENT TYPE");
						if(compType.length() == 0) {
							ctxObj.put("COMPONENT TYPE", componentType);
						}
					} else {
						ctxObj.put("COMPONENT TYPE", componentType);
					}

					System.out.println("Component Type:"+componentType);
					if(ctxObj.has("LINKED")) {
						String linked = (String) ctxObj.get("LINKED");
						if(linked.length() == 0) {
							ctxObj.put("LINKED", "N");
						}
					} else {
						ctxObj.put("LINKED", "N");
					}
				
					// Insert into enterprise_rel
					if(type != null && type.length() > 0) {
						pst1 = conn.prepareStatement(insertSQL);
						pst1.setString(1, componentId);
						pst1.setString(2, assetId);
						pst1.setString(3, type);
						pst1.setString(4, ctxObj.toString());
						pst1.setString(5, updatedBy);
						pst1.setString(6, updatedTimestamp);
						pst1.setString(7, updatedBy);
						pst1.setString(8, updatedTimestamp);
						pst1.setString(9, companyCode);

						pst1.executeUpdate();
						pst1.close();

						// Insert reverse relationship
						if(componentType != null && !componentType.equals(type)) {
							ctxObj.put("COMPONENT TYPE", type);
							ctxObj.put("LINKED", "Y");
							pst1 = conn.prepareStatement(insertSQL);
							pst1.setString(1, assetId);
							pst1.setString(2, componentId);
							pst1.setString(3, componentType);
							pst1.setString(4, ctxObj.toString());
							pst1.setString(5, updatedBy);
							pst1.setString(6, updatedTimestamp);
							pst1.setString(7, updatedBy);
							pst1.setString(8, updatedTimestamp);
							pst1.setString(9, companyCode);

							pst1.executeUpdate();
							pst1.close();
						}

						insertedCount++;
					}
				} else {
					foundCount++;

					OrderedJSONObject ctxObj = null;
					if(ctx != null) {
						ctxObj = new OrderedJSONObject(ctx);
					} else {
						ctxObj = new OrderedJSONObject();
					}
					ctxObj.put("COMPONENT TYPE", componentType);
					ctxObj.put("LINKED", "N");
					pst1 = conn.prepareStatement(updateSQL);
					pst1.setString(1, ctxObj.toString());
					pst1.setString(2, componentId);
					pst1.setString(3, assetId);
					pst1.executeUpdate();
					pst1.close();

					pst1 = conn.prepareStatement(checkSQL);
					pst1.setString(1, assetId);
					pst1.setString(2, componentId);
					rs1 = pst1.executeQuery();
					while (rs1.next()) {
						count = rs1.getInt("count");
					}
					rs1.close();
					pst1.close();

					if(count == 0 && componentType != null && !componentType.equals(type)) {
						ctxObj.put("COMPONENT TYPE", type);
						ctxObj.put("LINKED", "Y");
						pst1 = conn.prepareStatement(insertSQL);
						pst1.setString(1, assetId);
						pst1.setString(2, componentId);
						pst1.setString(3, componentType);
						pst1.setString(4, ctxObj.toString());
						pst1.setString(5, updatedBy);
						pst1.setString(6, updatedTimestamp);
						pst1.setString(7, updatedBy);
						pst1.setString(8, updatedTimestamp);
						pst1.setString(9, companyCode);

						pst1.executeUpdate();
						pst1.close();

						reverseCount++;
					}
				}
			} else {
				orphanCount++;
			}
		} 

		rs.close();
		pst.close();
		conn.commit();
		conn.close();

		System.out.println("Succesfully migrated Strategy Relationship: Inserted["+insertedCount+"] Found["+foundCount+"] Orphan["+orphanCount+"] Reverse["+reverseCount+"]!!!");
	}

	private void migrateAssetRelationship() throws Exception {
		int insertedCount = 0, foundCount = 0, orphanCount = 0, reverseCount = 0;
		String sql = "select * from enterprise_asset_rel";
		String checkSQL = "select count(*) count from enterprise_rel where component_id=? and asset_id=?";
		String checkAssetSQL = "select asset_category count from enterprise_asset where id = ?";
		String insertSQL = "insert into enterprise_rel(id,component_id,asset_id,asset_rel_type,asset_rel_context,created_by,created_timestamp,updated_by,updated_timestamp,company_code) values(null,?,?,?,?,?,?,?,?,?)";
		String updateSQL = "update enterprise_rel set asset_rel_context=? where component_id=? and asset_id=?";
		PreparedStatement pst = conn.prepareStatement(sql);
		ResultSet rs = pst.executeQuery();
		while (rs.next()) {
			String componentId = rs.getString("parent_asset_id");
			String assetId = rs.getString("child_asset_id");
			String type = rs.getString("asset_rel_type");
			String ctx = rs.getString("asset_rel_context");
			String updatedBy = rs.getString("updated_by");
			String updatedTimestamp = rs.getTimestamp("updated_timestamp").toString();
			String companyCode = rs.getString("company_code");

			System.out.println(componentId+":"+assetId+":"+type+":"+ctx);

			String componentType = checkRelationship(componentId);

			if(componentType != null) {
				int count = 0;
				PreparedStatement pst1 = conn.prepareStatement(checkSQL);
				pst1.setString(1, componentId);
				pst1.setString(2, assetId);
				ResultSet rs1 = pst1.executeQuery();
				while (rs1.next()) {
					count = rs1.getInt("count");
				}
				rs1.close();
				pst1.close();

				if(count == 0) {
					OrderedJSONObject ctxObj = null;
					if(ctx != null) {
						ctxObj = new OrderedJSONObject(ctx);
					} else {
						ctxObj = new OrderedJSONObject();
					}
					
					if(ctxObj.has("COMPONENT TYPE")) {
						String compType = (String) ctxObj.get("COMPONENT TYPE");
						if(compType.length() == 0) {
							ctxObj.put("COMPONENT TYPE", componentType);
						}
					} else {
						ctxObj.put("COMPONENT TYPE", componentType);
					}

					System.out.println("Component Type:"+componentType);
					if(ctxObj.has("LINKED")) {
						String linked = (String) ctxObj.get("LINKED");
						if(linked.length() == 0) {
							ctxObj.put("LINKED", "N");
						}
					} else {
						ctxObj.put("LINKED", "N");
					}
				
					// Insert into enterprise_rel
					if(type != null && type.length() > 0) {
						pst1 = conn.prepareStatement(insertSQL);
						pst1.setString(1, componentId);
						pst1.setString(2, assetId);
						pst1.setString(3, type);
						pst1.setString(4, ctxObj.toString());
						pst1.setString(5, updatedBy);
						pst1.setString(6, updatedTimestamp);
						pst1.setString(7, updatedBy);
						pst1.setString(8, updatedTimestamp);
						pst1.setString(9, companyCode);

						pst1.executeUpdate();
						pst1.close();

						// Insert reverse relationship
						if(componentType != null && !componentType.equals(type)) {
							ctxObj.put("COMPONENT TYPE", type);
							ctxObj.put("LINKED", "Y");
							pst1 = conn.prepareStatement(insertSQL);
							pst1.setString(1, assetId);
							pst1.setString(2, componentId);
							pst1.setString(3, componentType);
							pst1.setString(4, ctxObj.toString());
							pst1.setString(5, updatedBy);
							pst1.setString(6, updatedTimestamp);
							pst1.setString(7, updatedBy);
							pst1.setString(8, updatedTimestamp);
							pst1.setString(9, companyCode);

							pst1.executeUpdate();
							pst1.close();
						}

						insertedCount++;
					}
				} else {
					foundCount++;

					// Update Context
					OrderedJSONObject ctxObj = null;
					if(ctx != null) {
						ctxObj = new OrderedJSONObject(ctx);
					} else {
						ctxObj = new OrderedJSONObject();
					}
					ctxObj.put("COMPONENT TYPE", componentType);
					ctxObj.put("LINKED", "N");
					pst1 = conn.prepareStatement(updateSQL);
					pst1.setString(1, ctxObj.toString());
					pst1.setString(2, componentId);
					pst1.setString(3, assetId);
					pst1.executeUpdate();
					pst1.close();

					// Insert Reverse Relationship
					pst1 = conn.prepareStatement(checkSQL);
					pst1.setString(1, assetId);
					pst1.setString(2, componentId);
					rs1 = pst1.executeQuery();
					while (rs1.next()) {
						count = rs1.getInt("count");
					}
					rs1.close();
					pst1.close();

					if(count == 0 && componentType != null && !componentType.equals(type)) {
						ctxObj.put("COMPONENT TYPE", type);
						ctxObj.put("LINKED", "Y");
						pst1 = conn.prepareStatement(insertSQL);
						pst1.setString(1, assetId);
						pst1.setString(2, componentId);
						pst1.setString(3, componentType);
						pst1.setString(4, ctxObj.toString());
						pst1.setString(5, updatedBy);
						pst1.setString(6, updatedTimestamp);
						pst1.setString(7, updatedBy);
						pst1.setString(8, updatedTimestamp);
						pst1.setString(9, companyCode);

						pst1.executeUpdate();
						pst1.close();

						reverseCount++;
					}
				}
			} else {
				orphanCount++;
			}
		} 

		rs.close();
		pst.close();
		conn.commit();
		conn.close();
		System.out.println("Succesfully migrated Asset Relationship: Inserted["+insertedCount+"] Found["+foundCount+"] Orphan["+orphanCount+"] Reverse["+reverseCount+"]!!!");
	}

	private String checkRelationship(String id) throws Exception {
		String checkBusinessCapabilitySQL = "select count(*) count from enterprise_business_capability where id = ?";
		String checkStrategySQL = "select count(*) count from enterprise_business_strategy where id = ?";
		String checkPlanSQL = "select plan_type from enterprise_strategy where id = ?";
		String checkAssetSQL = "select asset_category, asset_type from enterprise_asset where id = ?";
		int count = 0;
		String type = null, category=null;
		PreparedStatement pst = conn.prepareStatement(checkBusinessCapabilitySQL);
		pst.setString(1, id);
		ResultSet rs = pst.executeQuery();
		while(rs.next()) {
			count = rs.getInt("count");
		}
		rs.close();
		pst.close();

		if(count > 0) {
			type = "CAPABILITY";
		} else {
			pst = conn.prepareStatement(checkStrategySQL);
			pst.setString(1, id);
			rs = pst.executeQuery();
			while(rs.next()) {
				count = rs.getInt("count");
			}
			rs.close();
			pst.close();

			if(count > 0) {
				type = "STRATEGY";
			} else {
				pst = conn.prepareStatement(checkPlanSQL);
				pst.setString(1, id);
				rs = pst.executeQuery();
				while(rs.next()) {
					type = rs.getString("plan_type");
				}
				rs.close();
				pst.close();

				if(type == null) {
					pst = conn.prepareStatement(checkAssetSQL);
					pst.setString(1, id);
					rs = pst.executeQuery();
					while(rs.next()) {
						category = rs.getString("asset_category");
						type = rs.getString("asset_type");
					}
					rs.close();
					pst.close();

					if(category != null && (category.equals("APPLICATION") || category.equals("IT ASSET") || category.equals("SOFTWARE"))) {
						type = category;
					}
				}
			}
		}

		return type;
	}

	public static void main(String[] args) throws Exception {
		String dbEnv=null, catalog=null, command=null;
		if(args.length == 3) {
			dbEnv = args[0];
			catalog = args[1];
			command = args[2];
			SmarteUtil util = new SmarteUtil(dbEnv, catalog);
			if(command.equals("enterprise_strategy_rel")) {
				util.migrateStrategyRelationship();
			} else if(command.equals("enterprise_asset_rel")) {
				util.migrateAssetRelationship();
			} else {
				System.out.println("Command not supported!!!");
			}
		} else {
			System.out.println("Invalid arguments!!!");
		}
	}
}